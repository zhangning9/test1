# Demo演示
在demo演示中可以查看到客户在space平台中查看到的工位概况和工位分析
![image](../image/p4.png)  
### 工位概况  
客户可以通过登录space平台查看到多种形式呈现的工位概况
![image](../image/p5.png)
### 工位分析  
客户也可以在space平台中查看到按部门统计的工位分析
![image](../image/p6.png)